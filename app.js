const container = document.querySelector(".container");
const sections = gsap.utils.toArray(".container section");
const divs = gsap.utils.toArray(".container div");
const texts = gsap.utils.toArray(".anim");
// const mask = document.querySelector(".mask");

let scrollTween = gsap.to(sections, {
  xPercent: -100 * (sections.length - 1),
  ease: "none",
  scrollTrigger: {
    trigger: ".container",
    pin: true,
    scrub: 1,
    end: "+=4500",
    id: "container",
    //snap: 1 / (sections.length - 1),
    // markers: true,
  },
});

// let scrollTweenDiv = gsap.to(divs, {
//     xPercent: -100 * (sections.length - 1),
//     ease: "none",
//     scrollTrigger: {
//       trigger: ".container",
//       pin: true,
//       scrub: 1,
//       end: "+=3000",
//       id: "container",
//       //snap: 1 / (sections.length - 1),
//       markers: true,
//     },
//   });

console.log(1 / (sections.length - 1));

// whizz around the sections
sections.forEach((section) => {
  // grab the scoped text
  console.log(section);
  let text = section.querySelectorAll(".anim");

  // bump out if there's no items to animate
  if (text.length === 0) return;

  // thats right anythings
  gsap.set(".animate2", { x: 20, y: -40, opacity: 0 });
  gsap.to(".animate2", {
    y: 50,
    x: 20,
    duration: 2,
    opacity: 10,
    ease: "elastic.out",
    scrollTrigger: {
      trigger: ".sec1",
      start: "buttom buttom",
      toggleActions: "play none none none",
      containerAnimation: scrollTween,
      id: "animation-2",
      // markers: {
      //   startColor: "yellow",
      //   endColor: "red",
      //   fontWeight: "bold",
      // },
    },
  });
  //leftside grid animation
  gsap.set(".flower", { y: -20, x: 900 });
  gsap.set(".timersvg", { y: -20, x: 900 });
  gsap.set(".circle", { y: -20, x: 900 });

  gsap.to(".flower", {
    y: 10,
    x: 180,
    duration: 2,
    ease: "circ.inOut",
    scrollTrigger: {
      trigger: ".sec3",
      start: "center center",
      toggleActions: "play none none reverse",
      id: "flower",
      containerAnimation: scrollTween,
      // markers: {
      //   startColor: "yellow",
      //   endColor: "green",
      // },
    },
  });

  gsap.to(".circle", {
    x: -0,
    y: 80,
    duration: 2,
    ease: "circ.inOut",
    scrollTrigger: {
      containerAnimation: scrollTween,
      trigger: ".sec3",
      start: "center center",
      toggleActions: "play none none reverse",
      id: "timer",
      // markers: {
      //   startColor: "yellow",
      //   endColor: "red",
      // },
    },
  });

  gsap.to(".timersvg", {
    y: -80,
    x: 0,
    duration: 2,
    ease: "circ.inOut",
    scrollTrigger: {
      containerAnimation: scrollTween,
      trigger: ".sec3",
      start: "center center",
      toggleActions: "play none none reverse",
      id: "timer",
      // markers: {
      //   startColor: "yellow",
      //   endColor: "red",
      // },
    },
  });

  // do a little stagger animation other section
  gsap.from(text, {
    y: -130,
    opacity: 0,
    duration: 2,
    ease: "elastic",
    stagger: 0.1,
    scrollTrigger: {
      trigger: section,
      containerAnimation: scrollTween,
      start: "left center",
      id: "text-animation",
      toggleActions: "play none none reverse",
      // markers: {
      //   startColor: "yellow",
      //   endColor: "red",
      // },
    },
  });

  gsap.set(".nice-and", { y: -80, x: 100 });
  gsap.to(".nice-and", {
    y: 400,
    x: 80,
    duration: 2,
    ease: "elastic",
    scrollTrigger: {
      trigger: ".sec5",
      start: "center center",
      toggleActions: "play none none reverse",
      id: ":nice-and",
      containerAnimation: scrollTween,
      // markers: {
      //   startColor: "yellow",
      //   endColor: "green",
      // },
    },
  });

  gsap.set(".easy", { y: -80, x: 100 });
  gsap.to(".easy", {
    y: 350,
    x: 0,
    duration: 2,
    ease: "elastic.out",
    scrollTrigger: {
      trigger: ".sec6",
      start: "center center",
      toggleActions: "play none none reverse",
      id: ":easy",
      containerAnimation: scrollTween,
      // markers: {
      //   startColor: "yellow",
      //   endColor: "green",
      // },
    },
  });
});

gsap.set(".easing", { y: -80, x: 100 });
gsap.to(".easing", {
  y: 400,
  x: -70,
  rotation: 45,
  duration: 2,
  ease: "elastic.out",
  scrollTrigger: {
    trigger: ".sec6-1",
    start: "end center",
    toggleActions: "play none none reverse",
    id: ":easing",
    containerAnimation: scrollTween,
    // markers: {
    //   startColor: "yellow",
    //   endColor: "green",
    // },
  },
});

// gsap.set(".add", { y: -80, x: 100 });
// gsap.to(".add", {
//   y: -130,
//   opacity: 0,
//   duration: 2,
//   ease: "elastic",
//   stagger: 0.1,
//   scrollTrigger: {
//     trigger: ".sec6-1",
//     start: "40% center",
//     toggleActions: "play none none reverse",
//     id: ":add",
//     containerAnimation: scrollTween,
//     markers: {
//       startColor: "yellow",
//       endColor: "green",
//     },
//   },
// });
gsap.set(".add", { x: 250, y: 350 });
gsap.from(".add", {
  y: 400,
  opacity: 0,
  duration: 1,
  ease: "cir.in",
  stagger: 0.1,
  scrollTrigger: {
    trigger: ".sec6-1",
    containerAnimation: scrollTween,
    start: "40% center",
    toggleActions: "play none none reverse",
    id: ":add",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "red",
    // },
  },
});

// const animateTextSlipt = () => {
//   console.log("enter the transition");
//   let text = new SplitText("#perAnimate", {
//     type: "chars",
//   });

//   gsap.set("#perAnimate", { autoAlpha: 1 });
//   gsap.set(text.chars, { yPercentage: 100 });

//   gsap.to(text.chars, {
//     yPercentage: 0,
//     ease: "sine.out",
//     stagger: {
//       from: "center",
//       amount: 0.5,
//       ease: "power1.out",
//       toggleActions : 'play none none reverse'
//     },
//   });
// };

// gsap.set(".per", { x: 480, y: 230 });
// let text = new SplitText("#perAnimate", {
//   type: "chars",
// });

// let tween = gsap.to(".per", { x: 100 }),
//   st = ScrollTrigger.create({
//     containerAnimation: scrollTween,
//     trigger: ".sec6",
//     start: "top center",
//     end: "+=500",
//     animation: tween,
//     markers: {
//       startColor: "yellow",
//       endColor: "red",
//     },
//   });

// gsap.set("#perAnimate", { autoAlpha: 1 });
// gsap.set(text.chars, { yPercentage: 100 });

// gsap.to(text.chars, {
//   yPercentage: 0,
//   ease: "sine.out",
//   stagger: {
//     containerAnimation: scrollTween,
//     from: "center",
//     amount: 0.5,
//     ease: "power1.out",
//     toggleActions: "play none none reverse",
//     id: ":per",
//     markers: {
//       startColor: "yellow",
//       endColor: "red",
//     },
//   },
// });

gsap.set(".per", { x: 480, y: 230 });
gsap.from(".per", {
  y: 250,
  opacity: 0,
  duration: 1,
  ease: "cir.in",
  stagger: 0.1,
  scrollTrigger: {
    trigger: ".sec6-2",
    containerAnimation: scrollTween,
    start: "top center",
    toggleActions: "play none none reverse",
    id: ":per",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "red",
    // },
    stagger: () => {},
    // onEnter: () => {
    //   animateTextSlipt();
    // },
  },
});

gsap.set(".hand", { x: 700, y: -300 });
gsap.to(".hand", {
  y: 200,
  opacity: 10,
  duration: 1,
  ease: "cir.in",
  rotation: 15,
  stagger: 0.1,
  scrollTrigger: {
    trigger: ".sec6-2",
    containerAnimation: scrollTween,
    start: "top center",
    toggleActions: "play none none reverse",
    id: ":hand",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "red",
    // },
    stagger: () => {},
    // onEnter: () => {
    //   animateTextSlipt();
    // },
  },
});

gsap.set(".red-half-cir", { x: 700, y: 400, opacity: 0 });
gsap.to(".red-half-cir", {
  x: 700,
  y: 400,
  rotation: 25,
  opacity: 1,
  duration: 1,
  ease: "cir.in",
  rotation: 15,
  stagger: 0.1,
  scrollTrigger: {
    trigger: ".sec6-2",
    containerAnimation: scrollTween,
    start: "top center",
    toggleActions: "play none none reverse",
    id: ":red-half-cir",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "red",
    // },
    stagger: () => {},
    // onEnter: () => {
    //   animateTextSlipt();
    // },
  },
});

gsap.set(".to-your", { x: 1100, y: -20 });
gsap.from(".to-your", {
  y: 50,
  opacity: 0,
  duration: 1,
  ease: "cir.in",
  stagger: 0.1,
  scrollTrigger: {
    trigger: ".sec7",
    containerAnimation: scrollTween,
    start: "top center",
    toggleActions: "play none none reverse",
    id: ":to-your",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "red",
    // },
  },
});

gsap.set(".animation", { x: 0, y: 410 });
gsap.from(".animation", {
  y: 500,
  duration: 1,
  ease: "cir.in",
  opacity: 0,
  stagger: 0.1,
  scrollTrigger: {
    trigger: ".sec8",
    containerAnimation: scrollTween,
    start: "top center",
    toggleActions: "play none none reverse",
    id: ":animation",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "red",
    // },
  },
});

gsap.set(".huge", { y: 410, x: 0, opacity: 0, scale: 4 });
gsap.to(".huge", {
  x: 0,
  y: 410,
  duration: 1,
  opacity: 10,
  ease: "power2.out",
  scale: 0.8,
  scrollTrigger: {
    trigger: ".sec8",
    start: "top center",
    toggleActions: "play none none reverse",
    id: ":huge",
    containerAnimation: scrollTween,
    // markers: {
    //   startColor: "yellow",
    //   endColor: "green",
    // },
  },
});

gsap.set(".super", { y: 250, x: 1100, opacity: 0 });
gsap.to(".super", {
  x: 1100,
  y: 350,
  duration: 2,
  opacity: 10,
  rotation: 10,
  ease: "power.in",
  scrollTrigger: {
    trigger: ".sec9",
    start: "top center",
    toggleActions: "play none none reverse",
    id: ":super",
    containerAnimation: scrollTween,
    // markers: {
    //   startColor: "yellow",
    //   endColor: "green",
    // },
  },
});

gsap.set(".pap", { y: 500, x: 1100, opacity: 0, rotation: -60 });
gsap.to(".pap", {
  x: 1100,
  y: 450,
  duration: 2,
  rotation: -15,
  opacity: 1,
  ease: "power.in",
  scrollTrigger: {
    trigger: ".sec9",
    start: "top center",
    toggleActions: "play none none reverse",
    id: ":pap",
    containerAnimation: scrollTween,
    // markers: {
    //   startColor: "yellow",
    //   endColor: "green",
    // },
  },
});

gsap.set(".star", { y: 700, x: 0, opacity: 10 });

gsap.to(".star", {
  scrollTrigger: {
    opacity: 10,
    trigger: ".sec11",
    // endTrigger: ".sec12",
    // end: "+=500",
    start: "end center",
    // toggleActions: "play none pasue reverse",
    // toggleActions: "restart pause reverse none",
    id: ":star",
    containerAnimation: scrollTween,
    // markers: {
    //   startColor: "yellow",
    //   endColor: "green",
    // },
  },
  duration: 2,
  // x: 500,
  rotation: "360",
  repeat: -1,
  opacity: 1,
  ease: "power.in",
});

gsap.to(".star", {
  scrollTrigger: {
    opacity: 10,
    trigger: ".sec12",
    endTrigger: ".sec12",
    end: "+=300",
    start: "end center",
    // toggleActions: "play none pasue reverse",
    toggleActions: "restart pause reverse none",
    id: ":star-move",
    containerAnimation: scrollTween,
    // markers: {
    //   startColor: "white",
    //   endColor: "red",
    // },
  },
  duration: 2,
  x: 500,
  rotation: "360",
  repeat: -1,
  opacity: 1,
  ease: "power.in",
});

gsap.set(".an", { y: 0, x: 0, opacity: 0 });

gsap.to(".an", {
  opacity: 10,
  duration: 1,
  y: 100,
  opacity: 1,
  ease: "power.in",
  scrollTrigger: {
    containerAnimation: scrollTween,
    trigger: ".sec12",
    start: "center center",
    toggleActions: "play none none reset",
    id: ":animation-1",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "green",
    // },
  },
});
gsap.set(".an", { y: 100 });
gsap.to(".an", {
  duration: 2,
  y: 105,
  x: 101,
  delay: 1,
  ease: "power.in",
  scrollTrigger: {
    containerAnimation: scrollTween,
    trigger: ".sec12",
    start: "center center",
    toggleActions: "play none none reset",
    id: ":animation-2",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "green",
    // },
  },
});

gsap.set(".sn", { y: 0, x: 0, opacity: 0 });

gsap.to(".sn", {
  opacity: 10,
  duration: 1,
  x: 510,
  opacity: 1,
  ease: "power.in",
  scrollTrigger: {
    containerAnimation: scrollTween,
    trigger: ".sec13",
    start: "center center",
    toggleActions: "play none none reset",
    id: ":sequence-1",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "green",
    // },
  },
});

gsap.set(".snIcon", { y: 0, x: 0, opacity: 0 });

gsap.to(".snIcon", {
  opacity: 10,
  duration: 0.5,
  x: 400,
  delay: 1,
  opacity: 1,
  ease: "bounce.in",
  scrollTrigger: {
    containerAnimation: scrollTween,
    trigger: ".sec13",
    start: "center center",
    toggleActions: "play none none reset",
    id: ":sequence-1",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "green",
    // },
  },
});

gsap.set(".snap", { y: 440, x: 400, rotation: 0, opacity: 10 });

gsap.to(".snap", {
  opacity: 10,
  y: 440,
  rotation: 50,
  duration: 2,
  // delay : 1,
  repeat: -1,
  ease: "cir.out",
  scrollTrigger: {
    containerAnimation: scrollTween,
    trigger: ".sec13",
    start: "center center",
    toggleActions: "play none none none",
    id: ":snap-1",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "green",
    // },
  },
});

gsap.set(".snap-1", { y: 250, x: 500, rotation: -20, opacity: 10 });

gsap.to(".snap-1", {
  opacity: 0,
  y: 320,
  rotation: 0,
  duration: 2,
  // delay : 1,
  repeat: -1,
  ease: "cir.out",
  scrollTrigger: {
    containerAnimation: scrollTween,
    trigger: ".sec13",
    start: "center center",
    toggleActions: "play none none none",
    id: ":snap-1",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "green",
    // },
  },
});

gsap.set(".ease-text", { x: 100, y: 500, opacity: 0 });
gsap.to(".ease-text", {
  y: 400,
  duration: 1,
  ease: "cir.in",
  stagger: 0.1,
  opacity: 1,
  scrollTrigger: {
    trigger: ".sec10",
    containerAnimation: scrollTween,
    start: "top center",
    toggleActions: "play none none reverse",
    id: ":ease-or",
    // markers: {
    //   startColor: "yellow",
    //   endColor: "red",
    // },
  },
});
